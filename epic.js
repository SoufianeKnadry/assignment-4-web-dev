"use strict";

/**
 * Array to store image URLs.
 * @type {Array}
 */
let imagearray = [];
let isFormSubmitting = false;
/**
 * Main function to encapsulate the code and avoid polluting the global namespace.
 */
!(function () {
/**
 * Event listener for DOMContentLoaded to initialize date restrictions.
 */
document.addEventListener("DOMContentLoaded", Restrictdates);

/**
 * Event listener for DOMContentLoaded to set up the form and its handlers.
 * @param {Event} e - The DOMContentLoaded event.
 */
document.addEventListener("DOMContentLoaded", function (e) {
    const ul = document.getElementById("image-menu");
    let type = document.getElementById("type");
    let date;

    /**
     * Event listener for changes in the type dropdown.
     * @param {Event} e - The change event on the type dropdown.
     */
    type.addEventListener("change", (e) => Restrictdates(e));

    /**
     * Event listener for form submission.
     * @param {Event} e - The submit event on the request form.
     */

    document.getElementById("request_form").addEventListener("submit", (e) => submit(e, type, date, ul));
});

/**
 * Function to handle form submission, fetch images, and update the UI.
 * @param {Event} e - The submit event.
 * @param {HTMLSelectElement} type - The type dropdown element.
 * @param {string} date - The selected date.
 * @param {HTMLUListElement} ul - The unordered list element for images.
 */
function submit(e, type, date, ul) {
    e.preventDefault();
    ul.textContent = undefined;
    imagearray = [];
    type = document.getElementById("type").value;
    date = document.getElementById("date").value;
    
    /**
     * Fetch images based on the selected type and date.
     */
    fetch(`https://epic.gsfc.nasa.gov/api/${type}/date/${date},{}`)
        .then(response => {
            if (!response.ok) {
                displayNoImageFound();
                throw new Error("Error", { cause: response });
            }
            return response.json();
        })
        .then(obj => {
            ul.textContent = undefined;
            if (obj.length == 0) {
                displayNoImageFound();
            } else {
                obj.forEach((obj) => {
                    imagearray.push(obj.image);
                    makeLis(obj);
                })
                ul.addEventListener("click", (e) => displayImage(e, type, date, obj));
            }

        })
        .catch(err => {
            console.error("3)Error:", err);
        });
        
}

/**
 * Function to create list items and append them to the image list.
 * @param {Object} obj - retrieved object containing date and other details.
 */
function makeLis(obj) {
    //Create li's according to the template and append it to the ul
    const ul = document.getElementById("image-menu");
    const template = document.getElementById("image-menu-item");
    let li = template.content.cloneNode(true);
    let liTitle = li.querySelector("span");
    liTitle.textContent = obj.date;
    ul.appendChild(li);
}

/**
 * Function to display image and image details when an image is clicked.
 * @param {Event} e - The click event on an image list item.
 * @param {string} type - The selected image type.
 * @param {string} date - The selected date.
 * @param {Object[]} obj - Array of objects retrieved from the url.
 */
function displayImage(e, type, date, obj) {
    const everyli = document.querySelectorAll("li");

    // Check if the list element is an error message.
    if (e.target.textContent == "No planet images found") {
        return;
    }

    for (let i = 0; i < everyli.length; i++) {
        everyli[i].setAttribute("data-image-list-index", i);
    }
    const img = document.getElementById("earth-image");
    const li = e.target.closest("li");
    const earthdate = document.getElementById("earth-image-date");
    const earthtitle = document.getElementById("earth-image-title");
    let formattedDate = date.replace(/-/g, "/");
    const dataIndex = li.getAttribute("data-image-list-index");

    //Display image and the captions
    img.src = `https://epic.gsfc.nasa.gov/archive/${type}/${formattedDate}/jpg/${imagearray[dataIndex]}.jpg`;
    earthdate.textContent = obj[dataIndex].date;
    earthtitle.textContent = obj[dataIndex].caption;
}

/**
 * Function to display a message when no planet images are found.
 */
function displayNoImageFound() {
    const ul = document.getElementById("image-menu");
    const li = document.createElement("li");
    li.textContent = "No planet images found";
    ul.appendChild(li);
}

/**
 * Function to restrict date input based on the latest available date for the selected type.
 */
function Restrictdates() {
    const type = document.getElementById("type").value;
    const dateinput = document.getElementById("date");

    /**
     * Fetch all images for the selected type to determine the latest available date.
     */
    fetch(`https://epic.gsfc.nasa.gov/api/${type}/all`)
        .then(response => {
            if (response.ok) {
                console.log(`${type}`);
            }
            else {
                console.log("failure dates");
            }
            return response.json();
        })

        .then(obj => {
            let datearray = [];
            obj.forEach((item) => {
                //Convert the field item.date into a string and store it in the datearray
                const date = item.date.toString();
                datearray.push(date);
            })
            // Sort the array datearray in descending order and set the first value into a variable
            let highestArray = datearray.sort((a, b) => b - a);
            const highestdate = highestArray[0];
            // Set input validation
            dateinput.max = highestdate;
        })
        .catch(err => {
            console.error("3)Error:", err);
        });
}
}());